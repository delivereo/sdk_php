<?php


namespace Delivereo_sdk\enums;


class CancelReason
{
    const NOT_NEEDED_ANYMORE = 'NOT_NEEDED_ANYMORE';
    const TOO_MUCH_DELAY = 'TOO_MUCH_DELAY';
    const BETTER_OPTION = 'BETTER_OPTION';
    const OTHER = 'OTHER';
}