<?php


namespace Delivereo_sdk\enums;


class Language
{
    const SPANISH = 'es';
    const ENGLISH = 'en';
}