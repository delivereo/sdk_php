<?php


namespace Delivereo_sdk\enums;


class ServerEnvironment
{
    const DEVELOPMENT = 'http://dgamc.ddns.net:8080';
    const TEST = 'http://delivereo-test-advanced.us-east-1.elasticbeanstalk.com';
    const STAGING = 'https://delivereo-test.com';
    const PRODUCTION = 'https://delivereo.com';
}