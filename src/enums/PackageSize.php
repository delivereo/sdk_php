<?php


namespace Delivereo_sdk\enums;


class PackageSize
{
    const SMALL = 'SMALL';
    const MEDIUM = 'MEDIUM';
    const LARGE = 'LARGE';

}