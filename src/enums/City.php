<?php


namespace Delivereo_sdk\enums;


class City
{
    const QUITO = 'QUITO';
    const GUAYAQUIL = 'GUAYAQUIL';
    const SANGOLQUI = 'SANGOLQUI';
    const RIOBAMBA = 'RIOBAMBA';
    const AMBATO = 'AMBATO';
    const BABAHOYO = 'BABAHOYO';
    const IBARRA = 'IBARRA';
    const CUENCA = 'CUENCA';
    const MANTA = 'MANTA';
    const PORTOVIEJO = 'PORTOVIEJO';
    const SANTO_DOMINGO = 'SANTO_DOMINGO';
    const LATACUNGA = 'LATACUNGA';
    const ESMERALDAS = 'ESMERALDAS';
    const QUININDE = 'QUININDE';
    const PEDERNALES = 'PEDERNALES';
    const LA_CONCORDIA = 'LA_CONCORDIA';
    const OTAVALO = 'OTAVALO';
}