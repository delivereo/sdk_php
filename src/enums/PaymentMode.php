<?php


namespace Delivereo_sdk\enums;


class PaymentMode
{
    const CASH = 'CASH';
    const CREDIT_CARD = 'CREDIT_CARD';
    const WALLET = 'WALLET';
    const NONE = 'NONE';	
    const CHECK = 'CHECK';
    const AGREEMENT = 'AGREEMENT';
    const MIXED = 'MIXED';
}