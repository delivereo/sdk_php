<?php


namespace Delivereo_sdk\enums;


class ErrorCode
{
    const NOT_ENOUGH_DATA = 401;
    const INCORRECT_DATA = 402;
    const DATA_NOT_FOUND = 404;
    const FAILURE = 405;
    const SUCCESS = 200;
    const INCORRECT_CREDENTIALS = 204;
    const USER_MOBILE_NUMBER_EMPTY = 205;
    const USER_SUSPENDED = 206;
    const USER_DISABLED = 207;
    const USER_NOT_VERIFIED = 208;
}