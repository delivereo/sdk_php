<?php


namespace Delivereo_sdk\enums;


class RatingLowReason
{

    const TOO_SLOW = 'TOO_SLOW';
    const BAD_APPEARANCE = 'BAD_APPEARANCE';
    const BAD_BEHAVIOR = 'BAD_BEHAVIOR';
    const PACKAGE_IN_BAD_SHAPE = 'PACKAGE_IN_BAD_SHAPE';
    const OTHER = 'OTHER';
    const NONE = 'NONE';

}