<?php


namespace Delivereo_sdk\enums;


class TransportMode
{
    const NONE = 'none';
    const CAR = 'car';
    const MOTORCYCLE = 'motorcycle';
    const BICYCLE = 'bicycle';
    const WALK = 'WALK';
}