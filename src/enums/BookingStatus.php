<?php


namespace Delivereo_sdk\enums;


class BookingStatus
{
    const MANUAL_ASSIGN = -4;
    const TRANSFERRING_TO = -3;
    const NO_DRIVER_FOUND = -2;
    const CANCELED = -1;
    const CREATED = 0;
    const GOING_FIRST_POSITION = 1;
    const ARRIVED_FIRST_POSITION = 2;
    const BUYING_ITEM = 3;
    const GOING_OTHER_POSITION = 4;
    const ARRIVED_OTHER_POSITION = 5;
    const GOING_LAST_POSITION = 6;
    const ARRIVED_LAST_POSITION = 7;
    const CHARGE = 8;
    const RATING = 9;
    const FINISHED = 10;
}