<?php

namespace Delivereo_sdk;

use \Delivereo_sdk\BusinessConstants;
use \Delivereo_sdk\enums\PackageSize;

use \Delivereo_sdk\request\create\CreateBookingDomicilePharmacy;
use \Delivereo_sdk\request\rate\RateDriver;
use \Delivereo_sdk\request\login\SignIn;
use \Delivereo_sdk\request\retry\RetryBooking;
use \Delivereo_sdk\request\cancel\CancelBooking;
use \Delivereo_sdk\request\create\CreateBooking;
use \Delivereo_sdk\request\detail\DetailBooking;
use \Delivereo_sdk\request\login\TokenRenewal;
use \Delivereo_sdk\request\calculate\CalculateBooking;

use \GuzzleHttp\Psr7\Request;
use \GuzzleHttp\Client;

class BusinessClient
{
    private $serverEnvironment, $language;

    /**
     * BusinessClient constructor.
     * @param string $serverEnvironment
     * @param string $language
     */
    public function __construct($serverEnvironment, $language)
    {
        $this->serverEnvironment = $serverEnvironment;
        $this->language = $language;
    }

    /**
     * get() method
     * @param $attribute
     * @return mixed
     */
    public function __get($attribute)
    {
        return $this->$attribute;
    }

    /**
     * set() method
     * @param $attribute
     * @param $value
     */
    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }

    /**
     * @param string $apiKey
     * @param string $businessEmail
     * @param string $businessRuc
     * @return mixed
     */
    public function login($apiKey, $businessEmail, $businessRuc)
    {
        $body = new SignIn($this->language, $apiKey, $businessEmail, $businessRuc);

        return $this->getResult(
            BusinessConstants::LOGIN_URL, '', $body->toJson(), true
        );
    }

    /**
     * @param string $oldJwtToken
     * @param string $businessEmail
     * @return mixed
     */
    public function tokenRenewal($oldJwtToken, $businessEmail)
    {
        $body = new TokenRenewal($this->language, $oldJwtToken, $businessEmail);
        return $this->getResult(
            BusinessConstants::TOKEN_RENEWAL_URL, '', $body->toJson(), true
        );
    }
   
    /**
     * @param string $jwtToken
     * @param string $cityType
     * @param string $points
     * @return mixed
     */
    public function calculateBookingCostUsingPoints($jwtToken, $cityType, $points)
    {
        $body = new CalculateBooking($this->language, PackageSize::SMALL, $cityType, $points, []);
        return $this->getResult(BusinessConstants::CALCULATE_URL, $jwtToken, $body->toJson());
    }

    /**
     * @param string $jwtToken
     * @param string $cityType
     * @param string $addresses
     * @return mixed
     */
    public function calculateBookingCostUsingAddresses($jwtToken, $cityType, $addresses)
    {
        $body = new CalculateBooking($this->language, PackageSize::SMALL, $cityType, [], $addresses);
        return $this->getResult(BusinessConstants::CALCULATE_URL, $jwtToken, $body->toJson());
    }

    /**
     * @param string $jwtToken
     * @param string $cityType
     * @param string $points
     * @param string $description
     * @param float $orderTotal
     * @param string $order
     * @param string $shopName
     * @param string $shopIcon
     * @param string $bookingBusinessUserEmail
     * @param string $extraInstructions
     * @return mixed
     */
    public function createBookingUsingPoints($jwtToken, $cityType, $points, $description, $orderTotal, $order,
                                             $bookingBusinessUserEmail = '', $shopName = '', $shopIcon = '', $extraInstructions = '')
    {
        $body = new CreateBooking($this->language, PackageSize::SMALL, $cityType, $points, [], $description,
                '', $shopName, $shopIcon, $bookingBusinessUserEmail, $orderTotal, $extraInstructions, null, $order);
        return $this->getResult(BusinessConstants::CREATE_URL, $jwtToken, $body->toJson());
    }

    /**
     * @param string $jwtToken
     * @param string $cityType
     * @param string $addresses
     * @param string $description
     * @param float $orderTotal
     * @param string $order
     * @param string $shopName
     * @param string $shopIcon
     * @param string $bookingBusinessUserEmail
     * @param string $extraInstructions
     * @return mixed
     */
    public function createBookingUsingAddresses($jwtToken, $cityType, $addresses, $description, $orderTotal, $order,
                                                $bookingBusinessUserEmail = '', $shopName = '', $shopIcon = '', $extraInstructions = '')
    {
        $body = new CreateBooking($this->language, PackageSize::SMALL, $cityType, [], $addresses, $description,
                '', $shopName, $shopIcon, $bookingBusinessUserEmail, $orderTotal, $extraInstructions, null, $order);
        return $this->getResult(BusinessConstants::CREATE_URL, $jwtToken, $body->toJson());
    }

    /**
     * @param $jwtToken
     * @param $businessFirstBranchName
     * @param $businessBranchNamesToTransfer
     * @param $businessUserEmail
     * @param $orderReference
     * @param $clientPersonalId
     * @param $clientFullName
     * @param $clientAddress
     * @param $clientPaymentMode
     * @param $countryCode
     * @param null $clientMobileNumber
     * @param null $clientReference
     * @return mixed
     */
    public function createBookingDomicilePharmacy($jwtToken, $businessFirstBranchName, $businessBranchNamesToTransfer,
                                                  $businessUserEmail, $orderReference, $clientPersonalId, $clientFullName,
                                                  $clientAddress, $clientPaymentMode, $countryCode, $clientMobileNumber = null, $clientReference = null)
    {
        $body = new CreateBookingDomicilePharmacy($this->language, $businessFirstBranchName, $businessBranchNamesToTransfer,
            $businessUserEmail, $orderReference, $clientPersonalId, $clientFullName, $clientAddress, $clientPaymentMode, $countryCode, $clientMobileNumber, $clientReference);
        return $this->getResult(BusinessConstants::CREATE_URL_DOMICILE_PHARMACY_URL, $jwtToken, $body->toJson());
    }

    /**
     * @param string $jwtToken
     * @param integer $bookingId
     * @param string $cancelReason
     * @param string $cancelFeedback
     * @return mixed
     */
    public function cancelBookingById($jwtToken, $bookingId, $cancelReason, $cancelFeedback)
    {
        $body = new CancelBooking($this->language, $bookingId, $cancelReason, $cancelFeedback);
        return $this->getResult(BusinessConstants::CANCEL_URL, $jwtToken, $body->toJson());
    }

    /**
     * @param string $jwtToken
     * @param string $bookingId
     * @return mixed
     */
    public function retryBookingById($jwtToken, $bookingId)
    {
        $body = new RetryBooking($this->language, $bookingId);
        return $this->getResult(BusinessConstants::RETRY_URL, $jwtToken, $body->toJson());
    }

    /**
     * @param string $jwtToken
     * @param integer $bookingId
     * @param integer $rating
     * @param string $ratingLowReason
     * @param string $ratingLowFeedback
     * @return mixed
     */
    public function rateDriverForBookingById($jwtToken, $bookingId, $rating, $ratingLowReason, $ratingLowFeedback)
    {
        $body = new RateDriver($this->language, $bookingId, $rating, $ratingLowReason, $ratingLowFeedback);
        return $this->getResult(BusinessConstants::RATE_DRIVER_URL, $jwtToken, $body->toJson());
    }

    /**
     * @param string $jwtToken
     * @param string $bookingId
     * @return mixed
     */
    public function getBookingFullDetailById($jwtToken, $bookingId)
    {
        $body = new DetailBooking($this->language, $bookingId);
        return $this->getResult(BusinessConstants::DETAIL_URL, $jwtToken, $body->toJson());
    }

    /**
     * @param string $url
     * @param string $jwtToken
     * @param string $body
     * @param bool $basicAuth
     * @return mixed
     */
    private function getResult($url, $jwtToken, $body, $basicAuth = false)
    {
        if ($basicAuth) {
            $headers = 'Basic c2VydmljZXMubW9iaWxlQGRlbGl2ZXJlby5jb206ZGVsaXZlcmVvLyotK2FiYzEyMw==';
        } else {
            $headers = 'Bearer ' . $jwtToken;
        }

        $options = [
            'headers' => [
                'Authorization' => $headers,
                'Content-Type' => 'application/json',
            ],
            'body' => $body
        ];

        $client = new Client();
        $response = $client->post($this->serverEnvironment . $url, $options);

        $result = new \stdClass();
        $result->statusCode = $response->getStatusCode();
        $result->contents = json_decode($response->getBody()->getContents());

        return $result;
    }
}