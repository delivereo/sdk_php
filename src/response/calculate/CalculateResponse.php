<?php


namespace Delivereo_sdk\response\calculate;


use \Delivereo_sdk\base\BasicResponse;

class CalculateResponse extends BasicResponse
{
    private $farePrice, $itemsPrice, $ivaPercentage, $iva, $totalAmount, $estimatedTime;

    /**
     * CalculateResponse constructor.
     * @param BasicResponse $basicResponse
     * @param float $farePrice
     * @param float $itemsPrice
     * @param float $ivaPercentage
     * @param float $iva
     * @param float $totalAmount
     * @param string $estimatedTime
     */
    public function __construct($basicResponse, $farePrice, $itemsPrice, $ivaPercentage,
                                $iva, $totalAmount, $estimatedTime)
    {
        parent::__construct($basicResponse->title, $basicResponse->message, $basicResponse->status, $basicResponse->code);

        $this->farePrice = $farePrice;
        $this->itemsPrice = $itemsPrice;
        $this->ivaPercentage = $ivaPercentage;
        $this->iva = $iva;
        $this->totalAmount = $totalAmount;
        $this->estimatedTime = $estimatedTime;
    }

    /**
     * get() method
     * @param $attribute
     * @return mixed
     */
    public function __get($attribute)
    {
        return $this->$attribute;
    }

    /**
     * set() method
     * @param $attribute
     * @param $value
     */
    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }
}