<?php


namespace Delivereo_sdk\response\detail;


class BookingPointResponse
{
    private $pointId, $pointOrder, $senderRecipientName, $address, $reference, $pointInitialTime, $pointArrivalTime;

    /**
     * BookingPointResponse constructor.
     * @param int $pointId
     * @param int $pointOrder
     * @param string $senderRecipientName
     * @param string $address
     * @param string $reference
     * @param $pointInitialTime
     * @param $pointArrivalTime
     */
    public function __construct($pointId, $pointOrder, $senderRecipientName, $address, $reference, $pointInitialTime, $pointArrivalTime)
    {
        $this->pointId = $pointId;
        $this->pointOrder = $pointOrder;
        $this->senderRecipientName = $senderRecipientName;
        $this->address = $address;
        $this->reference = $reference;
        $this->pointInitialTime = $pointInitialTime;
        $this->pointArrivalTime = $pointArrivalTime;
    }

    /**
     * get() method
     * @param $attribute
     * @return mixed
     */
    public function __get($attribute)
    {
        return $this->$attribute;
    }

    /**
     * set() method
     * @param $attribute
     * @param $value
     */
    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }

}