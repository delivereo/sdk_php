<?php


namespace Delivereo_sdk\response\detail;


use \Delivereo_sdk\base\BasicResponse;
use \Delivereo_sdk\enums\BookingStatus;
use \Delivereo_sdk\enums\TransportMode;

class BookingResponse extends BasicResponse
{
    private $bookingId, $userId, $generalUserId, $driverId, $driverFirstName, $driverLastName, $driverEmail;
    private $driverMobileNumber, $driverRegistrationNumber, $driverTransport, $driverTransportModeName;
    private $firstPointSender, $firstPointAddress, $firstPointInitialTime, $firstPointArrivalTime, $lastPointRecipient;
    private $lastPointAddress, $lastPointInitialTime, $lastPointArrivalTime, $fare, $description, $receiptStatus;
    private $bookingStatus, $bookingStatusName, $itemsPrice, $insuredAmount, $iva, $totalAmount, $storeName;
    private $otherImage, $orderReference, $totalDuration, $extraInstructions, $currentExtraPointId;
    private $publicGuid, $publicUrl, $extraPoints;

    /**
     * BookingResponse constructor.
     * @param BasicResponse $basicResponse
     * @param int $bookingId
     * @param int $userId
     * @param int $generalUserId
     * @param int $driverId
     * @param string $driverFirstName
     * @param string $driverLastName
     * @param string $driverEmail
     * @param string $driverMobileNumber
     * @param string $driverRegistrationNumber
     * @param TransportMode $driverTransport
     * @param string $driverTransportModeName
     * @param string $firstPointSender
     * @param string $firstPointAddress
     * @param $firstPointInitialTime
     * @param $firstPointArrivalTime
     * @param string $lastPointRecipient
     * @param string $lastPointAddress
     * @param $lastPointInitialTime
     * @param $lastPointArrivalTime
     * @param float $fare
     * @param string $description
     * @param BookingStatus $bookingStatus
     * @param string $bookingStatusName
     * @param float $itemsPrice
     * @param float $insuredAmount
     * @param float $iva
     * @param float $totalAmount
     * @param string $storeName
     * @param string $otherImage
     * @param string $orderReference
     * @param string $totalDuration
     * @param string $extraInstructions
     * @param int $currentExtraPointId
     * @param string $publicGuid
     * @param string $publicUrl
     * @param array $extraPoints
     */
    public function __construct(BasicResponse $basicResponse, $bookingId, $userId, $generalUserId, $driverId,
                                $driverFirstName, $driverLastName, $driverEmail, $driverMobileNumber,
                                $driverRegistrationNumber, $driverTransport, $driverTransportModeName,
                                $firstPointSender, $firstPointAddress, $firstPointInitialTime, $firstPointArrivalTime,
                                $lastPointRecipient, $lastPointAddress, $lastPointInitialTime, $lastPointArrivalTime,
                                $fare, $description, $bookingStatus, $bookingStatusName, $itemsPrice,
                                $insuredAmount, $iva, $totalAmount, $storeName, $otherImage, $orderReference,
                                $totalDuration, $extraInstructions, $currentExtraPointId, $publicGuid, $publicUrl,
                                $extraPoints)
    {
        parent::__construct($basicResponse->title, $basicResponse->message, $basicResponse->status, $basicResponse->code);

        $this->bookingId = $bookingId;
        $this->userId = $userId;
        $this->generalUserId = $generalUserId;
        $this->driverId = $driverId;
        $this->driverFirstName = $driverFirstName;
        $this->driverLastName = $driverLastName;
        $this->driverEmail = $driverEmail;
        $this->driverMobileNumber = $driverMobileNumber;
        $this->driverRegistrationNumber = $driverRegistrationNumber;
        $this->driverTransport = $driverTransport;
        $this->driverTransportModeName = $driverTransportModeName;
        $this->firstPointSender = $firstPointSender;
        $this->firstPointAddress = $firstPointAddress;
        $this->firstPointInitialTime = $firstPointInitialTime;
        $this->firstPointArrivalTime = $firstPointArrivalTime;
        $this->lastPointRecipient = $lastPointRecipient;
        $this->lastPointAddress = $lastPointAddress;
        $this->lastPointInitialTime = $lastPointInitialTime;
        $this->lastPointArrivalTime = $lastPointArrivalTime;
        $this->fare = $fare;
        $this->description = $description;
        $this->bookingStatus = $bookingStatus;
        $this->bookingStatusName = $bookingStatusName;
        $this->itemsPrice = $itemsPrice;
        $this->insuredAmount = $insuredAmount;
        $this->iva = $iva;
        $this->totalAmount = $totalAmount;
        $this->storeName = $storeName;
        $this->otherImage = $otherImage;
        $this->orderReference = $orderReference;
        $this->totalDuration = $totalDuration;
        $this->extraInstructions = $extraInstructions;
        $this->currentExtraPointId = $currentExtraPointId;
        $this->publicGuid = $publicGuid;
        $this->publicUrl = $publicUrl;
        $this->extraPoints = $extraPoints;
    }

    /**
     * get() method
     * @param $attribute
     * @return mixed
     */
    public function __get($attribute)
    {
        return $this->$attribute;
    }

    /**
     * set() method
     * @param $attribute
     * @param $value
     */
    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }
}