<?php


namespace Delivereo_sdk\response\create;


use \Delivereo_sdk\base\BasicResponse;

class CreateResponse extends BasicResponse
{
    private $bookingId;

    /**
     * CreateResponse constructor.
     * @param BasicResponse $basicResponse
     * @param int $bookingId
     */
    public function __construct($basicResponse, $bookingId)
    {
        parent::__construct($basicResponse->title, $basicResponse->message, $basicResponse->status, $basicResponse->code);

        $this->bookingId = $bookingId;
    }

    /**
     * get() method
     * @param $attribute
     * @return mixed
     */
    public function __get($attribute)
    {
        return $this->$attribute;
    }

    /**
     * set() method
     * @param $attribute
     * @param $value
     */
    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }
}