<?php


namespace Delivereo_sdk\response\login;


use \Delivereo_sdk\base\BasicResponse;

class SignInResponse extends BasicResponse
{
    private $id, $ruc, $businessName, $email, $countryCode, $mobileNumber, $businessImage, $walletBalance, $jwtToken;

    /**
     * SignInResponse constructor.
     * @param BasicResponse $basicResponse
     * @param int $id
     * @param string $ruc
     * @param string $businessName
     * @param string $email
     * @param string $countryCode
     * @param string $mobileNumber
     * @param string $businessImage
     * @param float $walletBalance
     * @param string $jwtToken
     */
    public function __construct($basicResponse, $id, $ruc, $businessName, $email, $countryCode,
                                $mobileNumber, $businessImage, $walletBalance, $jwtToken)
    {
        parent::__construct($basicResponse->title, $basicResponse->message, $basicResponse->status, $basicResponse->code);

        $this->id = $id;
        $this->ruc = $ruc;
        $this->businessName = $businessName;
        $this->email = $email;
        $this->countryCode = $countryCode;
        $this->mobileNumber = $mobileNumber;
        $this->businessImage = $businessImage;
        $this->walletBalance = $walletBalance;
        $this->jwtToken = $jwtToken;

    }

    /**
     * get() method
     * @param $attribute
     * @return mixed
     */
    public function __get($attribute)
    {
        return $this->$attribute;
    }

    /**
     * set() method
     * @param $attribute
     * @param $value
     */
    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }
}