<?php


namespace Delivereo_sdk\response\login;


use \Delivereo_sdk\base\BasicResponse;

class TokenRenewalResponse extends BasicResponse
{
    private $result;

    public function __construct(BasicResponse $basicResponse, $result)
    {
        parent::__construct($basicResponse->title, $basicResponse->message, $basicResponse->status, $basicResponse->code);

        $this->result = $result;

    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }
}