<?php

namespace Delivereo_sdk;

class BusinessConstants
{
    const LOGIN_URL = '/api/protected/login/business';
    const TOKEN_RENEWAL_URL = '/api/protected/token-renewal/business';
    const VALIDATE_ADDRESS_URL = '/api/private/business-bookings/validate-address';
    const CALCULATE_URL = '/api/private/business-bookings/calculate';
    const CREATE_URL = '/api/private/business-bookings/create';
    const CREATE_URL_DOMICILE_PHARMACY_URL = '/api/private/business-bookings/create-domicile';
    const CANCEL_URL = '/api/private/business-bookings/cancel';
    const RETRY_URL = '/api/private/business-bookings/retry';
    const RATE_DRIVER_URL = '/api/private/business-bookings/rate-driver';
    const DETAIL_URL = '/api/private/business-bookings/detail-full';
}