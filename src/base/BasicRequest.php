<?php


namespace Delivereo_sdk\base;

class BasicRequest
{
    protected $lang;

    /**
     * BasicRequest constructor.
     * @param $lang
     */
    public function __construct($lang)
    {
        $this->lang = $lang;
    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }
}