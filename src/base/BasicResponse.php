<?php


namespace Delivereo_sdk\base;


class BasicResponse
{
    private $title, $message, $status, $code;

    /**
     * BasicResponse constructor.
     * @param string $title
     * @param string $message
     * @param bool $status
     * @param string $code
     */
    public function __construct($title, $message, $status, $code)
    {
        $this->title = $title;
        $this->message = $message;
        $this->status = $status;
        $this->code = $code;
    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }
}