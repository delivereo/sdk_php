<?php


namespace Delivereo_sdk\request\login;


use \Delivereo_sdk\base\BasicRequest;

class SignIn extends BasicRequest
{
    private $apiKey, $email, $ruc;

    /**
     * SignIn constructor.
     * @param Language $lang
     * @param string $apiKey
     * @param string $email
     * @param string $ruc
     */
    public function __construct($lang, $apiKey, $email, $ruc)
    {
        parent::__construct($lang);

        $this->apiKey = $apiKey;
        $this->email = $email;
        $this->ruc = $ruc;
    }

    public function toJson()
    {
        return json_encode([
            'apiKey' => $this->apiKey,
            'email' => $this->email,
            'ruc' => $this->ruc,
            'lang' => $this->lang
        ]);
    }
}