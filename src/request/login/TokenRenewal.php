<?php


namespace Delivereo_sdk\request\login;


use \Delivereo_sdk\base\BasicRequest;

class TokenRenewal extends BasicRequest
{
    private $oldJwtToken, $email;

    /**
     * TokenRenewal constructor.
     * @param Language $lang
     * @param string $oldJwtToken
     * @param string $email
     */
    public function __construct($lang, $oldJwtToken, $email)
    {
        parent::__construct($lang);

        $this->oldJwtToken = $oldJwtToken;
        $this->email = $email;
    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }

    public function toJson()
    {
        return json_encode([
            'oldJwtToken' => $this->oldJwtToken,
            'email' => $this->email,
            'lang' => $this->lang
        ]);
    }

}