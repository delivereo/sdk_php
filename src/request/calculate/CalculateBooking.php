<?php


namespace Delivereo_sdk\request\calculate;


use \Delivereo_sdk\base\BasicRequest;

class CalculateBooking extends BasicRequest
{
    private $categoryType, $cityType, $points, $addresses;

    /**
     * CalculateBooking constructor.
     * @param Language $lang
     * @param PackageSize $categoryType
     * @param City $cityType
     * @param $points
     * @param $addresses
     */
    public function __construct($lang, $categoryType, $cityType, $points, $addresses)
    {
        parent::__construct($lang);

        $this->categoryType = $categoryType;
        $this->cityType = $cityType;
        $this->points = $points;
        $this->addresses = $addresses;
    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }

    public function toJson()
    {
        return json_encode([
            'cityType' => $this->cityType,
            'points' => $this->points,
            'addresses' => $this->addresses,
            'lang' => $this->lang
        ]);
    }
}