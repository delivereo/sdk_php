<?php


namespace Delivereo_sdk\request\calculate;


class CalculateAddress
{
    private $addressOrder;
    private $fullAddress, $addressMainStreet, $addressCrossingStreet, $countryCode;

    /**
     * CalculateAddress constructor.
     * @param int $addressOrder
     * @param string $fullAddress
     * @deprecated string $addressMainStreet
     * @deprecated string $addressCrossingStreet
     * @param string $countryCode
     */
    public function __construct($addressOrder, $fullAddress, $addressMainStreet, $addressCrossingStreet, $countryCode)
    {
        $this->fullAddress = $fullAddress;
        $this->addressOrder = $addressOrder;
        $this->addressMainStreet = $addressMainStreet;
        $this->addressCrossingStreet = $addressCrossingStreet;
        $this->countryCode = $countryCode;
    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }
}