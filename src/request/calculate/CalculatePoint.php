<?php


namespace Delivereo_sdk\request\calculate;


class CalculatePoint
{
    private $pointOrder, $pointLatitude, $pointLongitude;

    /**
     * CalculatePoint constructor.
     * @param int $pointOrder
     * @param $pointLatitude
     * @param $pointLongitude
     */
    public function __construct($pointOrder, $pointLatitude, $pointLongitude)
    {
        $this->pointOrder = $pointOrder;
        $this->pointLatitude = $pointLatitude;
        $this->pointLongitude = $pointLongitude;
    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }
}