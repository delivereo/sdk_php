<?php


namespace Delivereo_sdk\request\cancel;


use \Delivereo_sdk\base\BasicRequest;

class CancelBooking extends BasicRequest
{
    private $bookingId, $cancelReason, $cancelFeedback;

    /**
     * CancelBooking constructor.
     * @param Language $lang
     * @param int $bookingId
     * @param CancelReason $cancelReason
     * @param string $cancelFeedback
     */
    public function __construct($lang, $bookingId, $cancelReason, $cancelFeedback)
    {
        parent::__construct($lang);

        $this->bookingId = $bookingId;
        $this->cancelReason = $cancelReason;
        $this->cancelFeedback = $cancelFeedback;
    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }

    public function toJson()
    {
        return json_encode([
            'bookingId' => $this->bookingId,
            'cancelReason' => $this->cancelReason,
            'cancelFeedback' => $this->cancelFeedback,
            'lang' => $this->lang
        ]);
    }
}