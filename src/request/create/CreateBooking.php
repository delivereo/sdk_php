<?php


namespace Delivereo_sdk\request\create;


use \Delivereo_sdk\base\BasicRequest;

class CreateBooking extends BasicRequest
{
    private $categoryType, $cityType, $points, $addresses, $description, $bookingImage, $shopName, $shopIcon;
    private $bookingBusinessUserEmail, $itemsPrice, $extraInstructions, $maxSuggestedTime, $order;

    public function __construct($lang, $categoryType, $cityType, $points, $addresses, $description, $bookingImage, $shopName, $shopIcon,
                                $bookingBusinessUserEmail, $itemsPrice, $extraInstructions, $maxSuggestedTime, $order)
    {
        parent::__construct($lang);

        $this->categoryType = $categoryType;
        $this->cityType = $cityType;
        $this->points = $points;
        $this->addresses = $addresses;
        $this->description = $description;
        $this->bookingImage = $bookingImage;
        $this->shopName = $shopName;
        $this->shopIcon = $shopIcon;
        $this->bookingBusinessUserEmail = $bookingBusinessUserEmail;
        $this->itemsPrice = $itemsPrice;
        $this->extraInstructions = $extraInstructions;
        $this->maxSuggestedTime = $maxSuggestedTime;
        $this->order = $order;
    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }

    public function toJson()
    {
        return json_encode([
            'bookingImage' => $this->bookingImage,
            'categoryType' => $this->categoryType,
            'cityType' => $this->cityType,
            'description' => $this->description,

            'extraInstructions' => $this->extraInstructions,
            'itemsPrice' => $this->itemsPrice,
            'lang' => $this->lang,
            'maxSuggestedTime' => $this->maxSuggestedTime,

            'order' => $this->order,
            'points' => $this->points,
            'addresses' => $this->addresses,
            'shopIcon' => $this->shopIcon,
            'shopName' => $this->shopName,
            'bookingBusinessUserEmail' => $this->bookingBusinessUserEmail
        ]);
    }

}