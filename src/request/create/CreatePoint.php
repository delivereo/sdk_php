<?php


namespace Delivereo_sdk\request\create;


class CreatePoint
{
    private $pointOrder, $senderRecipientName, $address, $reference, $pointLatitude, $pointLongitude;

    public function __construct($pointOrder, $senderRecipientName, $address, $reference, $pointLatitude, $pointLongitude)
    {
        $this->pointOrder = $pointOrder;
        $this->pointLatitude = $pointLatitude;
        $this->pointLongitude = $pointLongitude;
        $this->senderRecipientName = $senderRecipientName;
        $this->address = $address;
        $this->reference = $reference;
    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }
}