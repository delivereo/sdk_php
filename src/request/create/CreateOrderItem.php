<?php


namespace Delivereo_sdk\request\create;


class CreateOrderItem
{
    private $orderItemGuid, $orderItemName, $orderItemDescription, $quantity, $unitSubTotal, $unitIva;
    private $unitTotal, $orderItemPriceTotal;

    public function __construct($orderItemGuid, $orderItemName, $orderItemDescription, $quantity, $unitSubTotal, $unitIva,
                                $unitTotal, $orderItemPriceTotal)
    {
        $this->orderItemGuid = $orderItemGuid;
        $this->orderItemName = $orderItemName;
        $this->orderItemDescription = $orderItemDescription;
        $this->quantity = $quantity;
        $this->unitSubTotal = $unitSubTotal;
        $this->unitIva = $unitIva;
        $this->unitTotal = $unitTotal;
        $this->orderItemPriceTotal = $orderItemPriceTotal;
    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }

    public function toArray()
    {
        return [
            'orderItemGuid' => $this->orderItemGuid,
            'orderItemName' => $this->orderItemName,
            'orderItemDescription' => $this->orderItemDescription,
            'quantity' => $this->quantity,
            'unitSubTotal' => $this->unitSubTotal,
            'unitIva' => $this->unitIva,
            'unitTotal' => $this->unitTotal,
            'orderItemPriceTotal' => $this->orderItemPriceTotal
        ];
    }

}