<?php


namespace Delivereo_sdk\request\create;


use Delivereo_sdk\base\BasicRequest;

class CreateBookingDomicilePharmacy extends BasicRequest
{
    private $businessFirstBranchName, $businessBranchNamesToTransfer, $businessUserEmail, $orderReference;
    private $clientPersonalId, $clientFullName, $clientAddress, $clientPaymentMode, $countryCode;
    private $clientMobileNumber, $clientReference;

    public function __construct($lang, $businessFirstBranchName, $businessBranchNamesToTransfer, $businessUserEmail, $orderReference,
                                $clientPersonalId, $clientFullName, $clientAddress, $clientPaymentMode, $countryCode,
                                $clientMobileNumber, $clientReference)
    {
        parent::__construct($lang);

        $this->businessFirstBranchName = $businessFirstBranchName;
        $this->businessBranchNamesToTransfer = $businessBranchNamesToTransfer;
        $this->businessUserEmail = $businessUserEmail;
        $this->orderReference = $orderReference;
        $this->clientPersonalId = $clientPersonalId;
        $this->clientFullName = $clientFullName;
        $this->clientAddress = $clientAddress;
        $this->clientPaymentMode = $clientPaymentMode;
        $this->countryCode = $countryCode;
        $this->clientMobileNumber = $clientMobileNumber;
        $this->clientReference = $clientReference;
    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }

    public function toJson()
    {
        return json_encode([
            'lang' => $this->lang,
            'businessFirstBranchName' => $this->businessFirstBranchName,
            'businessBranchNamesToTransfer' => $this->businessBranchNamesToTransfer,
            'businessUserEmail' => $this->businessUserEmail,
            'orderReference' => $this->orderReference,
            'clientPersonalId' => $this->clientPersonalId,
            'clientFullName' => $this->clientFullName,
            'clientAddress' => $this->clientAddress,
            'clientPaymentMode' => $this->clientPaymentMode,
            'countryCode' => $this->countryCode,
            'clientReference' => $this->clientReference,
            'clientMobileNumber' => $this->clientMobileNumber
        ]);
    }
}