<?php


namespace Delivereo_sdk\request\create;


class CreateAddress
{
    private $addressOrder;
    private $fullAddress, $addressMainStreet, $addressCrossingStreet, $countryCode, $senderRecipientName, $address, $reference;

    /**
     * CreateAddress constructor.
     * @param int $addressOrder
     * @param string $fullAddress
     * @deprecated  string $addressMainStreet
     * @deprecated  string $addressCrossingStreet
     * @param string $countryCode
     * @param string $senderRecipientName
     * @param string $address
     * @param string $reference
     */
    public function __construct($addressOrder, $fullAddress, $addressMainStreet, $addressCrossingStreet,
                                $countryCode, $senderRecipientName, $address, $reference)
    {
        $this->addressOrder = $addressOrder;
        $this->fullAddress = $fullAddress;
        $this->addressMainStreet = $addressMainStreet;
        $this->addressCrossingStreet = $addressCrossingStreet;
        $this->countryCode = $countryCode;
        $this->senderRecipientName = $senderRecipientName;
        $this->address = $address;
        $this->reference = $reference;
    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }
}