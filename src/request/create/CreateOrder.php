<?php


namespace Delivereo_sdk\request\create;


class CreateOrder
{
    private $orderGuid, $orderSubTotal, $paymentMode, $orderIva, $orderTotal, $orderItems;

    public function __construct($orderGuid, $orderSubTotal, $paymentMode, $orderIva, $orderTotal, $orderItems)
    {
        $this->orderGuid = $orderGuid;
        $this->orderItems = $orderItems;
        $this->orderSubTotal = $orderSubTotal;
        $this->orderIva = $orderIva;
        $this->orderTotal = $orderTotal;
        $this->paymentMode = $paymentMode;
    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }

    public function toArray()
    {
        return [
            'orderGuid' => $this->orderGuid,
            'orderItems' => $this->orderItems,
            'orderSubTotal' => $this->orderSubTotal,
            'orderIva' => $this->orderIva,
            'orderTotal' => $this->orderTotal,
            'paymentMode' => $this->paymentMode
        ];
    }

}