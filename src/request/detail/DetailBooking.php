<?php


namespace Delivereo_sdk\request\detail;


use \Delivereo_sdk\base\BasicRequest;

class DetailBooking extends BasicRequest
{
    private $bookingId;

    /**
     * DetailBooking constructor.
     * @param Language $lang
     * @param int $bookingId
     */
    public function __construct($lang, $bookingId)
    {
        parent::__construct($lang);

        $this->bookingId = $bookingId;
    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }

    public function toJson()
    {
        return json_encode([
            'bookingId' => $this->bookingId,
            'lang' => $this->lang
        ]);
    }
}