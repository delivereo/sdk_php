<?php


namespace Delivereo_sdk\request\rate;


use \Delivereo_sdk\base\BasicRequest;

class RateDriver extends BasicRequest
{
    private $bookingId;
    private $rating;
    private $ratingLowReasonType;
    private $ratingLowFeedback;

    public function __construct($lang, $bookingId, $rating, $ratingLowReasonType, $ratingLowFeedback)
    {
        parent::__construct($lang);

        $this->bookingId = $bookingId;
        $this->rating = $rating;
        $this->ratingLowReasonType = $ratingLowReasonType;
        $this->ratingLowFeedback = $ratingLowFeedback;
    }

    public function __get($attribute)
    {
        return $this->$attribute;
    }

    public function __set($attribute, $value)
    {
        $this->$attribute = $value;
    }

    public function toJson()
    {
        return json_encode([
            'bookingId' => $this->bookingId,
            'rating' => $this->rating,
            'ratingLowReasonType' => $this->ratingLowReasonType,
            'ratingLowFeedback' => $this->ratingLowFeedback,
            'lang' => $this->lang
        ]);
    }
}