# Delivereo SDK PHP

Delivereo PHP SDK Library

This is the official SDK library used to integrate with Delivereo's API.
You can find more information about our API in the [swagger documentation link](https://www.delivereo.com/swagger-ui.html)
or in the API [docs documentation link](https://delivereo.docs.stoplight.io)

## Installation

Install the latest version with

```bash
$ composer require delivereo/delivereo-sdk-php @dev
```

## Basic Usage / Example

```php
<?php

include_once 'vendor/autoload.php';

use \Delivereo_sdk\BusinessClient;
use \Delivereo_sdk\enums\ServerEnvironment;
use \Delivereo_sdk\enums\Language;
use \Delivereo_sdk\enums\City;
use \Delivereo_sdk\enums\PaymentMode;
use \Delivereo_sdk\enums\RatingLowReason;
use \Delivereo_sdk\enums\CancelReason;

use \Delivereo_sdk\request\create\CreateOrder;
use \Delivereo_sdk\request\create\CreateOrderItem;

try {

    /**
     * Crear una instancia del cliente de business. En server environment deberia apuntar a TEST
     */
    $businessClient = new BusinessClient(ServerEnvironment::TEST, Language::SPANISH);

    /**
     * Logearse con las credenciales
     */
    $loginResponse = $businessClient->login('69982032-DD5', 'info@delivereo.com', '1792715083001');

    /**
     * Si el login es exitoso, podemos usar el JWT token de la respuesta de login para poder ejecutar cualquier otra accion en el API
     */
    if ($loginResponse->statusCode == 200) {

        /**
         * TOMAR EN CUENTA: Si el JWT token expira (al mandar una peticion que no sea login o renew token te devuelve codigo 403).
         * Se puede generar un nuevo token volviendose a logear o usando el metodo siguiente
         */
        $renewTokenResponse = $businessClient->tokenRenewal($loginResponse->contents->jwtToken, 'info@delivereo.com');

        /*if ($renewTokenResponse->statusCode == 200) {
            // echo 'Nuevo jwt Token: ' . $renewTokenResponse->contents->jwtToken;
        }*/

        /**
         * Calcular el costo de una carrera usando puntos geograficos
         */
        $calculatePoints = calculateBookingWithPoints($businessClient, $loginResponse->contents->jwtToken);

        /**
         * Calcular el costo de una carrera usando direcciones
         */
        $calculateAddress = calculateBookingWithAddresses($businessClient, $loginResponse->contents->jwtToken);

        /**
         * No es necesario calcular el costo de una carrera si se va a crear una. Es solo referencial
         * De igual manera se puede crear una carrera usando puntos geograficos o direcciones, la mejor opcion es
         * usar direcciones por facilidad
         */

        /**
         * Crear carrera usando puntos geograficos
         */
        $createWithPoints = createBookingWithPoints($businessClient, $loginResponse->contents->jwtToken);

        /**
         * Despues de que la carrera fue atendida por un deliverer, esta puede ser calificada usando el
         * siguiente metodo. No es obligatorio el calificar el conductor, pero se recomienda hacerlo si es
         * posible. En una nueva version del API se va a generar una calificacion automatica, quitando esta
         * opcion
         */
        $rateBooking = rateBooking($businessClient, $loginResponse->contents->jwtToken, $createWithAddress->contents->bookingId);

        /**
         * Para obtener el detalle de una carrera se puede utilizar el siguiente metodo
         */
        $detailBooking = detailBooking($businessClient, $loginResponse->contents->jwtToken, $createWithPoints->contents->bookingId);

        /**
         * Si una carrera no puede ser atendida por alguno de nuestro delivererers, se puede reintentar usando el siguiente metodo
         */
        $retryBooking = retryBooking($businessClient, $loginResponse->contents->jwtToken, 28191);

        /**
         * Para cancelar una carrera, se puede utilizar el siguiente metodo
         */
        $cancelBooking = cancelBooking($businessClient, $loginResponse->contents->jwtToken, $createWithPoints->contents->bookingId);
      
        /**
         * Crear booking Domicilio
         */
        $createBookingDomicile = createBookingDomicile($businessClient, $loginResponse->contents->jwtToken);
        
        /**
         * Crear booking Farmacia
         */
        $createBookingDomicilePharmacy = createBookingDomicilePharmacy($businessClient, $loginResponse->contents->jwtToken);
    }

} catch ( Exception $exception) {

}

```

### Calculate bookings with points

```php
<?php
/**
 * Calcular Bookings con Puntos
 * @param $businessClient
 * @param $jwtToken
 * @return mixed
 */
function calculateBookingWithPoints($businessClient, $jwtToken)
{
    $points = [];
    $points[] = [
        'pointOrder' => 1, 'pointLatitude' => -0.19045013, 'pointLongitude' => -78.48059853
    ];

    $points[] = [
        'pointOrder' => 2, 'pointLatitude' => -0.172213, 'pointLongitude' => -78.486467
    ];

    $points[] = [
        'pointOrder' => 3, 'pointLatitude' => -0.20963146, 'pointLongitude' => -78.43948563
    ];

    return $businessClient->calculateBookingCostUsingPoints($jwtToken, City::QUITO, $points);
    
}

```

### Calculate bookings with address

```php
<?php
/**
 * Calcular Bookings con direcciones
 * @param $businessClient
 * @param $jwtToken
 * @return mixed
 */
function calculateBookingWithAddresses($businessClient, $jwtToken)
{
    $addresses = [];
    $addresses[] = [
        'addressOrder' => 1, 'addressMainStreet' => 'Amazonas', 'addressCrossingStreet' => 'Veintimilla', 'countryCode' => 'EC'
    ];

    $addresses[] = [
        'addressOrder' => 2, 'addressMainStreet' => 'Francisco Dalmau', 'addressCrossingStreet' => 'Calle 1', 'countryCode' => 'EC'
    ];

    return $businessClient->calculateBookingCostUsingAddresses($jwtToken, City::QUITO, $addresses);

}

```

### Create bookings with points

```php
<?php
/**
 * Crear Bookings con Puntos
 * @param $businessClient
 * @param $jwtToken
 * @return mixed
 */
function createBookingWithPoints($businessClient, $jwtToken)
{
    $points = [];
    $points[] = [
        'pointOrder' => 1, 'pointLatitude' => -0.19045013, 'pointLongitude' => -78.48059853,
        'senderRecipientName' => 'Jorge Cardenas', 'address' => 'Amazonas y Veintimilla E32A-32',
        'reference' => 'Edificio Tez Apartamento 200'
    ];

    $points[] = [
        'pointOrder' => 2, 'pointLatitude' => -0.172213, 'pointLongitude' => -78.486467,
        'senderRecipientName' => 'Marcelo Bonilla', 'address' => 'Diego de Vazquez y Bellavista E34-23',
        'reference' => 'Condominio Carmen 2 casa 34'
    ];

    $points[] = [
        'pointOrder' => 3, 'pointLatitude' => -0.20963146, 'pointLongitude' =>-78.43948563,
        'senderRecipientName' => 'Pablo Mancero', 'address' => 'Naciones Unidas e Inaquito Esquina',
        'reference' => 'Edificio Metropolitan Oficina 409'
    ];

    $orderItem_1 = new CreateOrderItem('cod_123', 'hamburguesa pollo', 'Hamburguesa de pollo',
                                    2, 1, 12, 2, 2.25);

    $orderItem_2 = new CreateOrderItem('cod_124', 'hamburguesa carne', 'Hamburguesa de carne',
                                        2, 1, 12, 2, 2.25);

    $order = new CreateOrder('123456', 10, PaymentMode::CREDIT_CARD, 1.2, 11.2,
                                [$orderItem_1->toArray(), $orderItem_2->toArray()]);

    return $businessClient->createBookingUsingPoints($jwtToken, City::QUITO, $points,
                                    'Compra hamburguesa de pollo y hamburguesa de carne', 11.2, $order->toArray(), 'stefano@delivereo.com');

}

```

### Calculate bookings with address

```php
<?php
/**
 * Crear Bookings con Direcciones
 * @param $businessClient
 * @param $jwtToken
 * @return mixed
 */
function createBookingWithAddresses($businessClient, $jwtToken)
{
    $addresses = [];
    $addresses[] = [
        'addressOrder' => 1, 'addressMainStreet' => 'Amazonas', 'addressCrossingStreet' => 'Veintimilla',
        'countryCode'=>'EC', 'senderRecipientName' => 'Jorge Cardenas', 'address' => 'Amazonas y Veintimilla E32A-32',
        'reference' => 'Edificio Tez Apartamento 200'
    ];

    $addresses[] = [
        'addressOrder' => 2, 'addressMainStreet' => 'Francisco Dalmau', 'addressCrossingStreet' => 'Francisco Dalmau',
        'countryCode'=>'EC', 'senderRecipientName' => 'Daniel Mancero', 'address' => 'Francisco Dalmau y Calle 1 OE32',
        'reference' => 'Frente al Supermaxi'
    ];

    $order = new CreateOrder('123456', 10, PaymentMode::CREDIT_CARD, 1.2, 11.2, []);

    return $businessClient->createBookingUsingAddresses($jwtToken, City::QUITO, $addresses,
                                                        'Compra hamburguesa de pollo y hamburguesa de carne', 11.2, $order->toArray(), 'stefano@delivereo.com');

}

```

### Rate bookings

```php
<?php
/**
 * Calificacion del Bookings
 * @param $businessClient
 * @param $jwtToken
 * @param $bookingId
 * @return mixed
 */
function rateBooking($businessClient, $jwtToken, $bookingId)
{
    return $businessClient->rateDriverForBookingById($jwtToken, $bookingId, 0, RatingLowReason::NONE, 'Everything OK');
}

```

### Details bookings

```php
<?php
/**
 * Detalles del Bookings
 * @param $businessClient
 * @param $jwtToken
 * @param $bookingId
 * @return mixed
 */
function detailBooking($businessClient, $jwtToken, $bookingId)
{
    return $businessClient->getBookingFullDetailById($jwtToken, $bookingId);
}

```

### Retry bookings

```php
<?php
/**
 * Procesar de nuevo el Bookings
 * @param $businessClient
 * @param $jwtToken
 * @param $bookingId
 * @return mixed
 */
function retryBooking($businessClient, $jwtToken, $bookingId)
{
    return $businessClient->retryBookingById($jwtToken, $bookingId);
}

```

### Cancel bookings

```php
<?php
/**
 * Cancelar Bookings
 * @param $businessClient
 * @param $jwtToken
 * @param $bookingId
 * @return mixed
 */
function cancelBooking($businessClient, $jwtToken, $bookingId)
{
    return $businessClient->cancelBookingById($jwtToken, $bookingId, CancelReason::NOT_NEEDED_ANYMORE, 'Another option was found');
}

```

### Create Booking Domicile

```php
<?php
/**
 * Crear Booking Domicilio
 * @param $businessClient
 * @param $jwtToken
 * @return mixed
 */
function createBookingDomicile($businessClient, $jwtToken)
{
    return $businessClient->createBookingDomicilePharmacy($jwtToken, 'CCI Quito', [], 'stefano@delivereo.com', '12345',
                                '1715245933', 'Daniel Mancero', 'Amazonas y Naciones Unidas', PaymentMode::CREDIT_CARD, 'EC');
}
```

### Create Booking Pharmacy

```php
<?php
/**
 * Crear Booking Farmacia. Los parametros mobil y referencia del cliente no son obligatorios
 * @param $businessClient
 * @param $jwtToken
 * @return mixed
 */
function createBookingDomicile($businessClient, $jwtToken)
{
    
    $booingCreatedWithMobilAndReference = null;
    $booingCreatedWithoutMobilAndReference = null;
    
    /*
     * Crear booking con movil del Cliente y la referencia
     * 
     */
    $booingCreatedWithMobilAndReference = $businessClient->createBookingDomicilePharmacy($jwtToken, 'CCI Quito', ['Inca Quito'], 'stefano@delivereo.com', '12345',
                                                                           '1715245933', 'Daniel Mancero', 'Amazonas y Naciones Unidas', PaymentMode::CREDIT_CARD, 'EC',
                                                                           '+593-123456789', 'Edificio Argos');
    
    // OR
    
    
    /*
     * Crear booking sin movil del Cliente y la referencia
     * 
     */
    $booingCreatedWithoutMobilAndReference = $businessClient->createBookingDomicilePharmacy($jwtToken, 'CCI Quito', ['Inca Quito'], 'stefano@delivereo.com', '12345',
                                                                               '1715245933', 'Daniel Mancero', 'Amazonas y Naciones Unidas', PaymentMode::CREDIT_CARD, 'EC');
    
    return ($booingCreatedWithMobilAndReference != null) ? $booingCreatedWithMobilAndReference : $booingCreatedWithoutMobilAndReference;
}
```